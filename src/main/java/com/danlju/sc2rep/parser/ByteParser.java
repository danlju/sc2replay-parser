package com.danlju.sc2rep.parser;

import com.danlju.sc2rep.parser.mpq.MpqByteReader;
import com.danlju.sc2rep.parser.types.*;
import com.mundi4.mpq.MpqFile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.logging.*;

public class ByteParser {

    private ByteIndex byteIndex;
    private final static Logger logger = Logger.getLogger(ByteParser.class.getName());

    public ByteParser() {
        this.byteIndex = new ByteIndex();
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter());
        logger.addHandler(handler);
        logger.setLevel(Level.WARNING);
    }

    /**
     *
     * @param mpq
     * @param entry
     * @return
     */
    public ReplayKeyArray getArray(MpqFile mpq, String entry) {
        byte[] bytes = getBytes(mpq, entry);
        ReplayKeyArray parsedArray = new ReplayKeyArray();

        while (byteIndex.value() < bytes.length) {
            ReplayDataType value = parseValue(bytes);
            if (value != null) {
                parsedArray.value().add(value);
            }
        }

        //printArray(parsedArray, 1);
        return parsedArray;
    }

    private ReplayDataType parseValue(byte[] bytes) {
        if (this.byteIndex.value() >= bytes.length) {
            return null;
        }
        switch (bytes[this.byteIndex.value()]) {

            // 02 0a 50 69 6c 6c 65 => "Pille" (when decoded)
            case ByteConstants.BYTE_STRING: {
                int size  = (bytes[this.byteIndex.value() + 1] >> 1);
                ReplayString string = parseByteString(bytes, this.byteIndex.value() + 2, size);

                this.byteIndex.increaseBy(2 + string.byteSize());
                return string;
            }

            // 04 01 00 08 | 02 0A 50 69 6C 6C 65 | 06 2A | 06 A6 | 06 8D
            case ByteConstants.ARRAY: {
                int size = bytes[this.byteIndex.value() + 3] >> 1;

                this.byteIndex.increaseBy(4);
                return readArray(bytes, this.byteIndex.value(), size);
            }

            // 05 08 | 00 09 04 | 02 07 00 00 53 32 | 04 09 02 | 08 09 f2 bf 50
            case ByteConstants.KEY_ARRAY: {
                int size = bytes[this.byteIndex.value() + 1] >> 1;
                this.byteIndex.increaseBy(2);
                return parseKeyArray(bytes, size, this.byteIndex.value());
            }

            // 06 4C => 0100 1100, shift of the sign bit to get positive 38
            case ByteConstants.INTEGER_SINGLE_BYTE: {
                ReplayInteger singleByteInteger = parseSingleByteInteger(bytes, this.byteIndex.value() + 1);
                this.byteIndex.increaseBy(2);
                return singleByteInteger;
            }

            // 07 00 00 53 32, gives a positive (last bit is zero) 10649 (shift off the last bit: 0x2999)
            case ByteConstants.INTEGER_FOUR_BYTES: {
                ReplayInteger replayInteger = parseFourByteInteger(bytes, this.byteIndex.value() + 1);
                this.byteIndex.increaseBy(5);
                return replayInteger;
            }

            // Example: 09 f2 bf 50
            // the data bytes in bits are 1111 0010, 1011 1111, 0101 0000
            case ByteConstants.INTEGER_VLF: {
                ReplayInteger vlfInteger = parseVLF(bytes, this.byteIndex.value() + 1);
                this.byteIndex.increaseBy(1 + vlfInteger.byteSize());
                return vlfInteger;
            }

            default: {
                //logger.warning("[" + byteIndex.value() + "] " + "no match for '" + bytes[byteIndex.value()] + "'");
                this.byteIndex.increase();
                //return null;
            }
        }
        //logger.warning("UNKNOWN VALUE: " + bytes[byteIndex.value() - 1]);

        return null;
    }

    // 02 0a 50 69 6c 6c 65 => "Pille" (when decoded)
    public ReplayString parseByteString(byte[] bytes, int startIndex, int size){
        byte[] stringBytes = new byte[size];

        int arrayPos = 0;
        for (int i=startIndex; i<startIndex + size; i++) {
            stringBytes[arrayPos++] = bytes[i];
        }

        ReplayString replayString = new ReplayString();
        replayString.byteSize(arrayPos);
        replayString.value(new String(stringBytes, StandardCharsets.UTF_8));

        return replayString;
    }


    // 04 01 00 08 | 02 0A 50 69 6C 6C 65 | 06 2A | 06 A6 | 06 8D
    public ReplayArray readArray(byte[] bytes, int startingIndex, int size) {
        ReplayArray valueArray = new ReplayArray();

        while (valueArray.value().size() < size) {
            valueArray.value().add(parseValue(bytes));
        }

        return valueArray;
    }

    // 05 08 | 00 09 04 | 02 07 00 00 53 32 | 04 09 02 | 08 09 f2 bf 50
    private ReplayKeyArray parseKeyArray(byte[] bytes, int size, Integer byteIndex) {
        ReplayKeyArray valueArray = new ReplayKeyArray();

        while (valueArray.value().size() < size) {
            this.byteIndex.increase();
            valueArray.value().add(parseValue(bytes));
        }
        return valueArray;
    }

    // 06 4C => 0100 1100, shift of the sign bit to get positive 38
    private ReplayInteger parseSingleByteInteger(byte[] bytes, int i) {
        ReplayInteger singleByteInteger = new ReplayInteger();
        singleByteInteger.byteSize(1);
        singleByteInteger.value((int) bytes[byteIndex.value() + 1] >> 1);
        return singleByteInteger;
    }

    // 07 00 00 53 32, gives a positive (last bit is zero) 10649 (shift off the last bit: 0x2999)
    public ReplayInteger parseFourByteInteger(byte[] bytes, int startIndex) {
        byte[] theInt = new byte[4];
        theInt[0] = bytes[startIndex];
        theInt[1] = bytes[startIndex + 1];
        theInt[2] = bytes[startIndex + 2];
        theInt[3] = bytes[startIndex + 3];

        ReplayInteger replayInteger = new ReplayInteger();
        replayInteger.value(ByteBuffer.wrap(theInt).getInt());
        replayInteger.byteSize(4);

        return replayInteger;
    }

    // Example: 09 f2 bf 50
    // the data bytes in bits are 1111 0010, 1011 1111, 0101 0000
    public ReplayInteger parseVLF(byte[] bytes, int startIndex) {
        int count = 0;
        int result = 0;
        boolean done = false;
        ReplayInteger vlfInteger = new ReplayInteger();

        while (!done) {
            int index = startIndex + count;
            if ((bytes[index] & 0x80) > 0) {
                result += (bytes[index] & 0x7f) << (7 * count);
                count++;
            }else {
                result += bytes[index] << (7 * count);
                done = true;
            }
        }
        result = (int)Math.pow(-1, result & 0x1) * (result >> 1);

        vlfInteger.byteSize(count + 1);
        vlfInteger.value(result);

        return vlfInteger;
    }

    private byte[] getBytes(MpqFile mpq, String entry) {
        try {
            return MpqByteReader.getBytes(mpq, mpq.getEntry(entry));
        } catch (IOException e) {
            throw new RuntimeException("Unable to read bytes from MPQ entry");
        }
    }

    private void printArray(ReplayKeyArray details, int tabs) {
        String tabsStr = getTabs(tabs);
        for (ReplayDataType data : details.value()) {
            if (data instanceof ReplayKeyArray) {
                printArray((ReplayKeyArray) data, tabs + 1);
            }else if (data instanceof ReplayArray) {
                ReplayKeyArray newArray = new ReplayKeyArray();
                newArray.value(((ReplayArray) data).value());
                printArray(newArray, tabs + 1);
            }else {
                if (data != null)
                    System.out.println((tabsStr + data.value()));
            }
        }
    }

    private String getTabs(int tabs) {
        String tabsStr = "";
        for (int i=0;i<tabs; i++) {
            tabsStr += "-\t";
        }
        return tabsStr;
    }

}
