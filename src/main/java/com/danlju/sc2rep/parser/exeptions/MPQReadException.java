package com.danlju.sc2rep.parser.exeptions;

public class MPQReadException extends RuntimeException {

    public MPQReadException() {
    }

    public MPQReadException(String message) {
        super(message);
    }
}
