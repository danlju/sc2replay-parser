package com.danlju.sc2rep.parser;

public class ByteIndex {
    private int index = 0;

    public int value() {
        return this.index;
    }

    public void increase() {
        this.increaseBy(1);
    }

    public void increaseBy(int inc) {
        this.index += inc;
    }
}
