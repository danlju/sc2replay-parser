package com.danlju.sc2rep.parser.mpq;

import com.mundi4.mpq.MpqEntry;
import com.mundi4.mpq.MpqFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MpqByteReader {

    public static byte[] getBytes(MpqFile mpqFile, MpqEntry mpqEntry) throws IOException {
        int nRead;
        byte[] data = new byte[5];
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        InputStream is = mpqFile.getInputStream(mpqEntry);

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        return buffer.toByteArray();
    }
}
