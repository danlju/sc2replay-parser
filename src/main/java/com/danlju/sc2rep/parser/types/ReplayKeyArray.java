package com.danlju.sc2rep.parser.types;

import java.util.ArrayList;

public class ReplayKeyArray extends ReplayDataType<ArrayList<ReplayDataType>> {
    public ReplayKeyArray() {
        this.value(new ArrayList<>());
    }

    public int byteSize() {
        int size = 0;
        for (ReplayDataType type : this.value()) {
            size += type.byteSize();
        }
        return size;
    }
}
