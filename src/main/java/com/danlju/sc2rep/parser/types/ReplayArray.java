package com.danlju.sc2rep.parser.types;

import java.util.ArrayList;

public class ReplayArray extends ReplayDataType<ArrayList<ReplayDataType>> {
    public ReplayArray() {
        super();
        this.value(new ArrayList<>());
    }

    public int byteSize() {
        int size = 0;
        for (ReplayDataType type : value()) {
            size += type.byteSize();
        }
        return size;
    }
}
