package com.danlju.sc2rep.parser.types;

public abstract class ReplayDataType<T> {
    private T value;
    private int byteSize = 0;

    public T value() {
        return value;
    }

    public void value(T value) {
        this.value = value;
    }

    public int byteSize() {
        return byteSize;
    }

    public void byteSize(int size) {
        this.byteSize = size;
    }
}
