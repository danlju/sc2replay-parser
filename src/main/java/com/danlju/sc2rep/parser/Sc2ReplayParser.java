package com.danlju.sc2rep.parser;

import com.danlju.sc2rep.parser.exeptions.MPQReadException;
import com.danlju.sc2rep.parser.model.details.ReplayDetails;
import com.mundi4.mpq.MpqFile;

import java.io.File;
import java.io.IOException;

public class Sc2ReplayParser {

    private MpqFile mpq;
    private ByteParser byteParser;

    public Sc2ReplayParser(String file) {
        this(new File(file));
    }

    public Sc2ReplayParser(File file) {
        try {
            mpq = new MpqFile(file);
        } catch (IOException e) {
            throw new MPQReadException("Unable to read MPQ file " + file.getName());
        }
        this.byteParser = new ByteParser();
    }

    public ReplayDetails parseDetails() {
        return new DetailsParser().parse(
                byteParser.getArray(mpq, "replay.details")
        );
    }
}
