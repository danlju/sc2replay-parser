package com.danlju.sc2rep.parser.model.details;

public class Map {
    private String name;

    public Map(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Map{" +
                "name='" + name + '\'' +
                '}';
    }
}
