package com.danlju.sc2rep.parser.model.details;

import java.util.List;

public class ReplayDetails {
    private List<Team> teams;
    private List<Player> players;
    private Map map;

    public ReplayDetails(List<Team> teams, Map map) {
        this.teams = teams;
        this.map = map;
    }

    public List<Team> teams() {
        return teams;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Map map() {
        return map;
    }

    @Override
    public String toString() {
        return "ReplayDetails{" +
                "players=" + players +
                ", map=" + map +
                '}';
    }
}
