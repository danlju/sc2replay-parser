package com.danlju.sc2rep.parser.model.details;

import java.util.Arrays;

public class Player {
    private String name;
    private String race;
    private int[] color = new int[4];
    private boolean inWinningTeam;

    public Player(String name, String race, int[] color, boolean inWinningTeam) {
        this.name = name;
        this.race = race;
        this.color = color;
        this.inWinningTeam = inWinningTeam;
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int[] getColor() {
        return color;
    }

    public boolean isInWinningTeam() {
        return inWinningTeam;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", race='" + race + '\'' +
                ", color=" + Arrays.toString(color) +
                ", inWinningTeam=" + inWinningTeam +
                '}';
    }
}
