package com.danlju.sc2rep.parser.model.details;

import java.util.List;

public class Team {
    private int number;
    private List<Player> players;

    public Team(int number, List<Player> players) {
        this.number = number;
        this.players = players;
    }

    public int number() {
        return number;
    }

    public List<Player> players() {
        return players;
    }

    @Override
    public String toString() {
        return "Team{" +
                "number=" + number +
                ", players=" + players +
                '}';
    }
}
