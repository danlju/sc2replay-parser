package com.danlju.sc2rep.parser;

public class ByteConstants {

    private ByteConstants() {}

    public static final int BYTE_STRING = 0x2;
    public static final int ARRAY = 0x4;
    public static final int KEY_ARRAY = 0x5;
    public static final int INTEGER_SINGLE_BYTE= 0x6;
    public static final int INTEGER_FOUR_BYTES= 0x7;
    public static final int INTEGER_VLF = 0x9;
}

