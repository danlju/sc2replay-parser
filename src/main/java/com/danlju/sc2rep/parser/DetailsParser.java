package com.danlju.sc2rep.parser;

import com.danlju.sc2rep.parser.model.details.Map;
import com.danlju.sc2rep.parser.model.details.Player;
import com.danlju.sc2rep.parser.model.details.ReplayDetails;
import com.danlju.sc2rep.parser.model.details.Team;
import com.danlju.sc2rep.parser.types.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailsParser {

    public DetailsParser() {
    }

    // TODO: clean up
    public ReplayDetails parse(ReplayKeyArray array) {
        List detailsList = findDetailsList(array);

        ReplayArray playerArray = (ReplayArray) detailsList.get(0);
        List<Team> teams = parseTeams(playerArray);
        ReplayString mapString = (ReplayString) detailsList.get(1);

        return new ReplayDetails(teams, new Map(mapString.value()));
    }

    private List findDetailsList(ReplayKeyArray array) {
        List list = array.value();
        ReplayKeyArray keyArray = (ReplayKeyArray) list.get(0);
        return keyArray.value();
    }

    private List<Team> parseTeams(ReplayArray playerArray) {
        java.util.Map<Integer, List<Player>> teamsMap = new HashMap<>();

        playerArray.value().forEach(v -> {
            String name = parsePlayerName((ReplayKeyArray) v);
            int team = parsePlayerTeam((ReplayKeyArray) v); // TODO: correct variable?
            String race = parseRace((ReplayKeyArray) v);
            int[] color = parseColor((ReplayKeyArray) v);
            boolean inWinningTeam = playerInWinningTeam((ReplayKeyArray) v);

            if (!teamsMap.containsKey(team)) {
                teamsMap.put(team, new ArrayList<>());
            }
            teamsMap.get(team).add(new Player(name, race, color, inWinningTeam));
        });

        List<Team> teams = new ArrayList<>();
        teamsMap.forEach(
                (k,v) -> teams.add(new Team(k, v))
        );

        return teams;
    }

    private boolean playerInWinningTeam(ReplayKeyArray v) {
        return (Integer)v.value().get(8).value() == 1;
    }

    private String parseRace(ReplayKeyArray a) {
        return (String)a.value().get(2).value();
    }

    private int parsePlayerTeam(ReplayKeyArray a) {
        return (int)a.value().get(5).value() + 1;
    }

    private String parsePlayerName(ReplayKeyArray a) {
        String name = (String)a.value().get(0).value();
        return name.replace("&lt;", "<").replace("&gt;", ">").replace("<sp/>","");
    }

    @SuppressWarnings("unchecked")
    private int[] parseColor(ReplayKeyArray k) {
        List colorArray = (List<ReplayInteger>)k.value().get(3).value();
        int[] color = new int[4];
        color[0] = ((ReplayInteger)colorArray.get(0)).value();
        color[1] = ((ReplayInteger)colorArray.get(1)).value();
        color[2] = ((ReplayInteger)colorArray.get(2)).value();
        color[3] = ((ReplayInteger)colorArray.get(3)).value();

        return color;
    }
}
