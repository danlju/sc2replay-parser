package com.danlju.sc2rep.parser;

import java.io.File;

public class TestUtils {
    public static File getTestFile(String suffix) {
        return new File("testreplays/test" + suffix + ".SC2Replay");
    }
}
