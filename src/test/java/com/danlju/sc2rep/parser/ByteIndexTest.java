package com.danlju.sc2rep.parser;

import org.junit.Test;
import static org.junit.Assert.*;

public class ByteIndexTest {
    @Test
    public void indexShouldBeZeroWhenCreated() {
        ByteIndex byteIndex = new ByteIndex();
        assertEquals(0, byteIndex.value());
    }

    @Test
    public void verifyCorrectIncrement() {
        ByteIndex byteIndex = new ByteIndex();
        byteIndex.increase();
        assertEquals(1, byteIndex.value());
        byteIndex.increaseBy(5);
        assertEquals(6, byteIndex.value());
    }
}
