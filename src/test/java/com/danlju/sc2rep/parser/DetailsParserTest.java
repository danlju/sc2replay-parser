package com.danlju.sc2rep.parser;

import com.danlju.sc2rep.parser.model.details.ReplayDetails;
import com.danlju.sc2rep.parser.model.details.Team;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class DetailsParserTest {

    private Sc2ReplayParser parser;

    @Before
    public void beforeTest() {
        Sc2ReplayParser parser = new Sc2ReplayParser(TestUtils.getTestFile("6"));
    }

    @Test
    public void shouldReturnListOfTeams() throws IOException{
        givenParserWithTestFile("5");

        ReplayDetails details = parser.parseDetails();

        assertNotNull(details);

        for (Team team : details.teams()) {
            System.out.println(team.toString());
        }
        System.out.println(details.map().getName());

        assertTrue("Player array should not be empty", !details.teams().isEmpty());
    }

    @Test
    public void shouldReturnMap() {
        givenParserWithTestFile("5");
        ReplayDetails details = parser.parseDetails();
        assertNotNull(details);
        assertNotNull(details.map());
        assertNotNull(details.map().getName());
        assertTrue("Map should not be null/empty", !details.map().getName().isEmpty());
        System.out.println(details.map().getName());
    }

    public void givenParserWithTestFile(String testFileNumber) {
        parser = new Sc2ReplayParser(TestUtils.getTestFile(testFileNumber));
    }
}
